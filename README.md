Base role
=========

Role to do basic configuration of a Debian/Ubuntu server. This role does the following tasks:

* Updates packages and reboot the server if needed (for example if there was a kernel update).
* Installs packages from a list.
* Installs python pip packages.
* Configures the server timezone.
* Configures the hostname.
* Updates /etc/hosts file with all servers belonging to the play.

Requirements
------------

* Python 3.
* SSH


Role Variables
--------------

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

* The `packages` variable contains a list of packages that must be installed on all servers. Default value:

      packages:
        - nfs-common

* The `extra_packages` variables can be used to install additional packages on all servers.
* The `pip_packages` variable contains a list of python modules installed with pip that must be installed on all servers. Default value:

      pip_packages:
        - jsondiff
        - psutil
        - pyyaml

* `timezone`: Sets the server timezone. (Default: "America/Bogota")
* `reboot_server`: Reboot server if needed after an update that requires it (Default: False)
* `reboot_timeout`: Reboot timeout (Default: 420s)
* `reboot_delay`: Post reboot delay, time to give the server to finish booting up (Default: 120s)

Example Playbook
----------------

    - name: "Server provisioning"
      hosts:  all
      become: yes
      become_user: root
      roles:
        - base

      vars_files:
        - vars/base.yml

License
-------

GPLv2 or later

Author Information
------------------

* Juan Luis Baptiste < juan _at_ juanbaptiste _dot_ tech >
* https://www.juanbaptiste.tech
